package fi.vamk.e1701577.herokudemo;

import static org.assertj.core.api.Assertions.*;
import static org.junit.Assert.assertEquals;

import java.sql.Date;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Configuration
@ComponentScan(basePackages = {"fi.vamk.e1701577.herokudemo"})
@EnableJpaRepositories(basePackageClasses = AttendanceRepository.class)
public class AttendanceTests {

    @Autowired
    private AttendanceRepository repository;
    /*
     * Test case 1: program can save, fetch by key, delete attendance 
     */
    @Test
    public void postGetDeleteAttendance() {
        Iterable<Attendance> begin = repository.findAll();
        // given
        Attendance att = new Attendance("key");
        System.out.println("ATT: " + att.toString());
        // test save
        Attendance saved = repository.save(att);
        // when
        Attendance found = repository.findByKey(saved.getKey());
        // then
        assertThat(found.getKey()).isEqualTo(att.getKey());
        repository.delete(att);
        Iterable<Attendance> end = repository.findAll();
        assertEquals((long)IterableUtils.size(begin), (long)IterableUtils.size(end));
    }
    /*
     * Test case 2: program can save and fetch from repository by date
     */
    @Test
    public void testAttendanceWithDate() {
        // create a test case where you will

        // add an attendance
        Attendance att = new Attendance();
        // enter key
        att.setKey("Key");
        // and date
        Date date = new Date(new java.util.Date().getTime());
        att.setDate(date);

        // and save the attendance to database
        repository.save(att);

        // and then fetch it by the date
        Attendance found = repository.findByDate(att.getDate());

        System.out.println("att: " + att.toString());
        System.out.println("found:" + found.toString());

        // assert the objects by comparing the results of toString -methods.
        assertEquals(found.toString(), att.toString());
        repository.delete(att);
    }
}
