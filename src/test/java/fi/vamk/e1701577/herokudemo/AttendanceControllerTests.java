package fi.vamk.e1701577.herokudemo;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


@SpringBootTest
@AutoConfigureMockMvc
public class AttendanceControllerTests extends AbstractTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private AttendanceRepository repository;

    @WithMockUser("USER")
    @Test
    public void getList() throws Exception {
        Attendance att = new Attendance("XYZ");
        att = repository.save(att);

        MvcResult mvcResult = mvc.perform(
            MockMvcRequestBuilders.get("/attendance/" + att.getId())
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
            
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
    
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(super.mapToJson(att), content);
    }

    @WithMockUser("USER")
    @Test
    public void createAttendanceViaPost() throws Exception {
        
        Attendance att = new Attendance("ABCDE");
        String inputJson = super.mapToJson(att);
        att.setId(5);
        System.out.println(inputJson);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/attendance")
            .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(super.mapToJson(att), content);
    }
}