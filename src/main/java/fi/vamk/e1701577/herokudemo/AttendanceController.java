package fi.vamk.e1701577.herokudemo;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class AttendanceController {

    @Autowired
    private AttendanceRepository repository;

    @GetMapping("/attendances")
    public Iterable<Attendance> list() {
        return repository.findAll();
    }

    @GetMapping("/attendance/{id}")
    public Optional<Attendance> get(@PathVariable("id") int id) {
        return repository.findById(id);
    }

    @PostMapping("/attendance")
    public @ResponseBody Attendance create(@RequestBody Attendance item) {
        return repository.save(item);
    }

    @PutMapping("/attendance")
    public @ResponseBody Attendance update(@RequestBody Attendance item) {
        return repository.save(item);
    }

    @DeleteMapping("/attendance")
    public void delete(@RequestBody Attendance item) {
        repository.delete(item);
    }
}