package fi.vamk.e1701577.herokudemo;

import java.sql.Date;
import org.springframework.data.repository.CrudRepository;

public interface AttendanceRepository extends CrudRepository<Attendance, Integer> {
    public Attendance findByKey(String key);
    public Attendance findByDate(Date date);
}