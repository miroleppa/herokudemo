package fi.vamk.e1701577.herokudemo;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.*;

@Entity
@NamedQuery(name="Attendance.findAll", query="SELECT p FROM Attendance p")
public class Attendance implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String key;
    private Date date;

    public Attendance() {    }

    public Attendance(String key) {
        this.key = key;
    }
    public Attendance(String key, Date date) {
        this.key = key;
        this.date = date;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }

    public Date getDate() {
        return this.date;
    }
    public void setDate(Date date) {
        this.date = date;
    }

    public String toString() {
        return "ID: " + getId() + ", key: " + getKey() + ", date: " + getDate();
    }
}