package fi.vamk.e1701577.herokudemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class HerokudemoApplication {

	@Autowired
	private AttendanceRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(HerokudemoApplication.class, args);
	}

	@Bean
	public void initData() {
		Attendance att1 = new Attendance("QWERTY");
		Attendance att2 = new Attendance("ABC");
		Attendance att3 = new Attendance("XYZ");
		repository.save(att1);
		repository.save(att2);
		repository.save(att3);
	}
}
